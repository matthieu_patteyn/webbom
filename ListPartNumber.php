<?php
include_once 'conex.ini.php';
include_once "include.php";
include_once "classAccess.php";
include_once "classDeviceJson.php";
include_once "header.php";

PageHeader("ListPartNumber.php");

if($Access->index_php()!=0){
    
    $id_type=-1;
    if(isset($_GET["id_type"])){
        $id_type=protect($_GET["id_type"]);
    }
    $lib=-1;
    if(isset($_GET["lib"])){
        $lib=protect($_GET["lib"]);
    }
    
    $list=array();
    //$list_var=JSON_read_octopart_name($id_type);
    $list_mod_unit=JSON_read_unit($id_type);
    
    if($id_type!=-1){
        $WHERE=" id_type=\"$id_type\" ";
    }
    elseif ($lib!=-1){
        if($lib==""){
            $WHERE=" lib=\"$lib\" or lib is null ";
        }
        else{
            $WHERE=" lib=\"$lib\" ";
        }
        
    }
	elseif(isset($_GET["where"])){
		$w=protect($_GET["where"]);
		$w="%" . $w ."%";
		
		$WHERE=" Reference like \"$w\" OR internRef like \"$w\" OR description like \"$w\" ";
	}
    else{
        $WHERE=" 1 ";
    }
    
    $sql="SELECT idRef,internRef, Reference,description,Status,SMT,id_type FROM reference where $WHERE order by internRef ASC;";
    $res= query($sql);
    while($req=mysqli_fetch_array($res)){
        $l=array();
        $l+=["idRef" => $req["idRef"] ];
        $l+=["Reference" => $req["Reference"] ];
        $l+=["internRef" => $req["internRef"] ];
        $l+=["description" => $req["description"] ];
        $l+=["Obsolete" => $req["Status"] ];
        $l+=["SMT" => $req["SMT"] ];
        $l+=["id_type" => $req["id_type"] ];
        
        foreach ($list_mod_unit as $key => $value){
            $l+=[$key => ["val" => "","unit" => "" ]];
        }
        
        array_push($list,$l);
    }
    
    
    $sql="SELECT reference_spec.`idRef`, reference_spec.`name_spec`,reference_spec.`value_spec`, reference_spec.`unit_spec` FROM `reference_spec`, reference WHERE reference.idRef=reference_spec.idRef and reference.id_type=$id_type ";
    $res= query($sql);
    while($req=mysqli_fetch_array($res)){
        //find id_ref
        for($i=0;$i<count($list);$i++){
            if($list[$i]["idRef"]==$req["idRef"]){
                if(isset($list[$i][$req["name_spec"]])){
                    $list[$i][$req["name_spec"]]["val"]=$req["value_spec"];
                    if($req["value_spec"]!=""){
                        $list[$i][$req["name_spec"]]["unit"]=$req["unit_spec"];
                    }
                    else{
                        $list[$i][$req["name_spec"]]["unit"]="";
                    }
                    
                }
            }
        }
    }
    echo"<table width=\"100%\"><tr><td>";
    if($id_type!=-1){
        echo"Type:" .JSON_read_name($id_type);
    }
    elseif($lib!=-1){
        echo"Library: $lib";
    }
    else{
        echo"All";
        
    }
    
	echo"\n</td><td align=\"right\"><a href=\"ref.php?NewPartNumber=1&device_type=$id_type\" >New Part Number</a></td></tr></table>";
    echo"<table border=1 width=100%>";
    //table header
    echo"<tr><td>InternRef</td><td>Reference</td><td>Description</td>";
    if($id_type==-1){
        echo"<td>Device Type</td>";
    }
    echo"<td>SMT</td>";
    foreach ($list_mod_unit as $key =>$val){
        echo"<td>$key</td>";
    }
    
    echo "</tr>\n";
    
    for($i=0;$i<count($list);$i++){
        if($list[$i]["Obsolete"]==0 || $list[$i]["Obsolete"]==-1){
            echo"<tr>";
        }
        
        else{
            echo"<tr bgcolor=\"LightGray\">";
        }
        
        if ($list[$i]["Obsolete"]==-1){//Need to update
            echo"<td><b><a href=\"ref.php?ref=".$list[$i]["idRef"]."\">".$list[$i]["internRef"]."</a></b></td>";
            echo"<td><b><a href=\"ref.php?ref=".$list[$i]["idRef"]."\">".$list[$i]["Reference"]."</a></b></td>";
            echo"<b><td><b>".$list[$i]["description"]."</b></td></b>";
            
            
        }
        else{
            echo"<td><a href=\"ref.php?ref=".$list[$i]["idRef"]."\">".$list[$i]["internRef"]."</a></td>";
            echo"<td><a href=\"ref.php?ref=".$list[$i]["idRef"]."\">".$list[$i]["Reference"]."</a></td>";
            echo"<td>".$list[$i]["description"]."</td>";
        }
        
        
        if($id_type==-1){
            echo"<td><a href=\"ListPartNumber.php?id_type=".$list[$i]["id_type"]."\">".JSON_read_name($list[$i]["id_type"])."</a></td>";
        }
        echo"<td>".$list[$i]["SMT"]."</td>";
        foreach ($list_mod_unit as $key =>$val){
            if(isset($list_mod_unit[$key])){
                if($list[$i][$key]["unit"]=="Ω" && $list_mod_unit[$key]=="mΩ"){
                    echo"<td>".$list[$i][$key]["val"]*1000 ." ".$list_mod_unit[$key] ."</td>";
                }
                elseif($list[$i][$key]["unit"]=="Hz" && $list_mod_unit[$key]=="KHz"){
                    echo"<td>".$list[$i][$key]["val"]/1000 ." ".$list_mod_unit[$key] ."</td>";
                }
                else{
                    echo"<td>".$list[$i][$key]["val"] ." ".$list[$i][$key]["unit"] ."</td>";
                }
            }
            else{
                echo"<td>".$list[$i][$key]["val"] ." ".$list[$i][$key]["unit"] ."</td>";
            }
            
        }
        
        echo "</tr>\n";
    }
    
    
    
    
    echo"</table>";
}





?>