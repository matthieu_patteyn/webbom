<?php 

include_once "classAccess.php";
include_once "include.php";
include_once "classProject.php";

class classBOM{
	private $project;
	private $idBom=0;
	public $id_project=0;
	public $id_revision="";
	public $information="";
	public $date_revision="";
	private $readOnly=1;
	
	
	public function __construct($idBom) {
		global $Access;
		$this->project=new classProject(0);
		$this->idBom=0;
		if($Access->getLevel()>1){
			$idBom=protect($idBom);
			$this->idBom=$idBom;
			$this->update();
		}
		
	}
	
	private function update(){
		if($this->idBom!=0){
			$sql="SELECT id_project,id_revision,information,date_rev,readOnly FROM bom where idBom=\"".$this->idBom."\";";
			$res= query($sql);
			$req=mysqli_fetch_array($res);
			
			$this->id_project=$req[0];
			$this->id_revision=$req[1];
			$this->information=$req[2];
			$this->date_revision=$req[3];
			$this->readOnly=$req[4];
		}
		$this->project=new classProject($this->id_project);
		
	}
	
	public function getid_bom(){
		return $this->idBom;
		
	}
	
	public function getReadOnly(){
		return $this->readOnly;
	}
	public function setReadOnly(){
		if($this->idBom!=0){
			$sql="UPDATE bom SET readOnly = 1
			WHERE idBom=\"".$this->idBom."\"; ";
			query($sql);
			
			$this->update();
			
			
		}
		
		
	}
	
	public function getProjectName(){
		return $this->project->name;
	}
	
	public function getProjectDescription(){
		return $this->project->description;
	}
	
	
	public function updateInformation($information){
		$information=protect($information);
		
		$sql="UPDATE bom SET information = \"$information\"
		WHERE idBom=\"".$this->idBom."\"; ";
		query($sql);
		
		$this->update();
	}
	
	
	public function lineUpdateWarning($ligne,$value){
		$ligne=protect($ligne);
		$value=protect($value);
		if($this->idBom){
			$sql="UPDATE bom_list SET warning = \"$value\"
			WHERE idBom=\"".$this->idBom."\" and id=\"$ligne\"; ";
			query($sql);
			return 1;
		}
		return 0;
	}
	public function lineUpdateInfo($ligne,$value){
		$ligne=protect($ligne);
		$value=protect($value);
		if($this->idBom){
			$sql="UPDATE bom_list SET info = \"$value\"
			WHERE idBom=\"".$this->idBom."\" and id=\"$ligne\"; ";
			query($sql);
			return 1;
		}
		return 0;
	}
	
	public function findPart($ref){
		$ref=protect($ref);
		$sql="SELECT idRef FROM reference where internRef=\"$ref\";";
		$res= query($sql);
		if($req=mysqli_fetch_array($res)){
			return $req[0];
		}
		return 0;
			
	}
	public function listAddReference($references,$quantity,$part){
		$references=protect($references);
		$quantity=protect($quantity);
		$part=protect($part);
		
		if($part!=0 and $this->readOnly==0){
			
			$sql="INSERT INTO bom_list (idBom,idRef,ref,quantity)
			VALUES (\"".$this->idBom."\",\"$part\",\"$references\",\"$quantity\");";
			query($sql);
			
		}
	}
	
	public function newRevision($id_project,$infoRevision="NewRevision"){
		$id_project=protect($id_project);
		$infoRevision=protect($infoRevision);
		global $Access;
		if($Access->getLevel()>1){
			$id_revision=0;
			$sql="SELECT max(id_revision) FROM bom where id_project=\"$id_project\";";
			$res= query($sql);
			if($req=mysqli_fetch_array($res)){
				$id_revision=$req[0]+1;
			}
			
			$sql="INSERT INTO bom (id_project,id_revision,information,date_rev)
			VALUES (\"".$id_project."\",\"$id_revision\",\"$infoRevision\",now());";
			query($sql);
			
			$sql="SELECT max(idBom) FROM bom where id_project=\"$id_project\";";
			$res= query($sql);
			if($req=mysqli_fetch_array($res)){
				$this->idBom=$req[0];
				$this->update();
				return $this->idBom;
			}
			
		}
		return 0;
		
		
	}
	
	
}



?>