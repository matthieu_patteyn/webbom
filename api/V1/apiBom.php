<?php
include_once "../../conex.ini.php";
include_once "../../include.php";
include_once "../../classAccess.php";
include_once "../../classBOM.php";
$BomRef=0;
if(isset($_GET["BomRef"])){
	$BomRef=$_GET["BomRef"];
}
if(isset($_GET["bomRef"])){
	$BomRef=$_GET["bomRef"];
}
$bom=new classBOM($BomRef);

$action="help";
if(isset($_GET["action"])){
	$action=$_GET["action"];
}
$response = array();


switch($action){
	case "help":
	default:
		$response["help"]="<h1>BOM API</h1>
				<h2>Requiere:</h2>
					BomRef=Value<br>
				<h2>action=</h2>
					<h3>update_warning</h3>
						ligne=<br>value= 
					<h3>update_info</h3>
						ligne=<br>value= ";
		break;
	case "update_warning":
		if(isset($_GET["ligne"]) and isset($_GET["value"])){
			$bom->lineUpdateWarning($_GET["ligne"],$_GET["value"]);
			$response["action"]="done!";
		}
		
		break;
	case "update_info":
		if(isset($_GET["ligne"]) and isset($_GET["value"])){
			$bom->lineUpdateInfo($_GET["ligne"],$_GET["value"]);
			$response["action"]="done!";
		}
		
		break;
	case "newBom":
		if(isset($_GET["ProjectName"]) and isset($_GET["ProjectDescription"]) and isset($_GET["bomInfo"])){
			if($Access->getLevel()>1){
				$project=new classProject();
				$id_project=$project->new_project($_GET["ProjectName"],$_GET["ProjectDescription"]);//New project if needed
				$response["id_project"]=$id_project;
				if($id_project){
					$bom->newRevision($id_project,$_GET["bomInfo"]);
					
					
				}
			}
			
			
			
		}
		
		break;
	
}


echo json_encode($response);


?>