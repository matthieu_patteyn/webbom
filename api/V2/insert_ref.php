<?php
include_once '../../conex.ini.php';
include_once "../../include.php";
include_once "../../classAccess.php";
include_once "../../classPart.php";

/*
 * Insert one reference & price
 * 
 * 
 */

/*
  Example : 
  
  Test 08/2018 OK 
  
{"action":"insert_ref",
"InternRef":"SMD-5048",
"Reference":"NE555",
"Description":"Timer NE555",
"SMT":"1",
"Manufacturer":"TI",
"ManufacturerPartNo":"NE555P",
"lib":"analog",
"price":[
{
"supplierName":"Farnell",
"supplierReference":"REF1",
"quantity":"100",
"UnitPrice":"0.1",
"Link":"http://"
},
{
"supplierName":"Farnell",
"supplierReference":"REF2",
"quantity":"1000",
"UnitPrice":"0.09",
"Link":"http://"
}

]
}



return : 


{"action":"Found"   ,"idRef":"1793"}   >> I have something 
{"action":"insert_ref"   ,"idRef":"1793"} >> 



  
 




 */

if($Access->getLevel()>0 && isset($json["action"])){
    if(isset($json["Reference"])){
        $Reference=protect($json["Reference"]);
        if($Reference!="" ){
            $part=new classPart();
            if($part->findReference($Reference)==0){
                $part->NewPartNumber();
                if(isset($json["Description"])){
                    $part->Description=$json["Description"];
                }
                if(isset($json["InternRef"])){
                    $part->internRef=$json["InternRef"];
                }
                $part->SMT="";
                if(isset($json["SMT"])){
                    $part->SMT=$json["SMT"];
                }
                $part->Manufacturer="";
                if(isset($json["Manufacturer"])){
                    $part->Manufacturer=$json["Manufacturer"];
                }
                $part->ManufacturerPartNo="";
                if(isset($json["ManufacturerPartNo"])){
                    $part->ManufacturerPartNo=$json["ManufacturerPartNo"];
                }
                $part->reference=$Reference;
                if(isset($json["lib"])){
                    $part->lib=$json["lib"];
                }
                if(isset($json["IDDeviceType"])){
                    $part->id_type=$json["IDDeviceType"];
                }
                
                
                
                $part->savePart();
                
                
                
                if(isset($json["price"])){
                    
                    foreach ($json["price"] as $price){
                        
                        if(isset($price["supplierName"]) && isset($price["supplierReference"]) && isset($price["quantity"])  && isset($price["UnitPrice"]) ){
                            $Link="";
                            if(isset($price["Link"])){
                                $Link=$price["Link"];
                            }
                            
                            
                            $part->priceAdd($price["supplierName"],$price["supplierReference"],$price["quantity"],$price["UnitPrice"],$Link);
                            
                            
                            
                        } 
                    }
                }
                echo"{\"action\":\"insert_ref\"   ,\"idRef\":\"".$part->getidRef() ."\"}";
                
            }
            else{
                echo"{\"action\":\"Found\"   ,\"idRef\":\"".$part->getidRef() ."\"}";
            }
        }  
    }
}
?>