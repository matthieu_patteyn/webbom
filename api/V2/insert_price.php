<?php
include_once '../../conex.ini.php';
include_once "../../include.php";
include_once "../../classAccess.php";
include_once "../../classPart.php";



/*
 Example :
 
 {"action":"insert_price",
 "id_device":"1",
 "price":
 {
 "supplierName":"Farnell",
 "supplierReference":"REF1",
 "quantity":"100",
 "UnitPrice":"0.1",
 "Link":"http://"
 }
 }
 
 
 */


if($Access->getLevel()>0 && isset($json["action"])){
    if(isset($json["id_device"])){
        $id_device=protect($json["id_device"]);
        if($id_device!="" ){
            $part=new classPart($id_device);
            
            if(isset($json["price"])){
                
                $price=$json["price"];
                
                
                $supplierName="";
                if(isset($price["supplierName"])){
                    $supplierName=$price["supplierName"];
                }
                $supplierReference="";
                if(isset($price["supplierReference"])){
                    $supplierReference=$price["supplierReference"];
                }
                $quantity=0;
                if(isset($price["quantity"])){
                    $quantity=$price["quantity"];
                }
                $UnitPrice=0;
                if(isset($price["UnitPrice"])){
                    $UnitPrice=$price["UnitPrice"];
                }
                $Link="";
                if(isset($price["Link"])){
                    $Link=$price["Link"];
                }
                
                
                
                $part->priceAdd($supplierName,$supplierReference,$quantity,$UnitPrice,$Link);

                
                echo"{\"action\":\"insert_price\"   ,\"idRef\":\"".$part->getidRef() ."\"}";
            }

        }
    }
}
?>