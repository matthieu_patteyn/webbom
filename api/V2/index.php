<?php
include_once '../../conex.ini.php';
include_once "../../include.php";
include_once "../../classAccess.php";
header('Content-Type: application/json');

if(isset($_GET["API_login"])){
    $Access->connectAPI($_GET["API_login"]);
}

if($Access->getLevel()>0){
    
    if(isset($_GET["queries"])){
        
        $json=json_decode($_GET["queries"],true);

        
        if(isset($json)){
            if(isset($json["action"])){
                switch($json["action"]){
                    case "insert_ref":
                        include("insert_ref.php");
                        break;
                    case "insert_price":
                        include("insert_price.php");
                        break;
                    case "test":
                        echo"test";
                        break;
                    case "ref_list":
                        include("ref_list.php");
                        break;
                    case "ref":
                        include("ref.php");
                        break;
                    default:
                        echo"unknown command";
                        break;
                        
                }  
                
            }
            else{
                $list=array();
                $list+=["error" => "No Action" ];
                echo  json_encode($list) ;
            }
            
        }  
        else{
            $list=array();
            $list+=["error" => "No JSON FILE" ];
            echo  json_encode($list) ;
        }
    }
	
}
?>