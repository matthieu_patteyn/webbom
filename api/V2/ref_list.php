<?php 
include_once '../../conex.ini.php';
include_once "../../include.php";
include_once "../../classAccess.php";
include_once "../../classPart.php";

/*
 Example :
 
 
 //send 
 {"action":"ref_list"
 }
 
 //return 
 
 
  {"action":"ref_list"
    "devices"[
    {
    "idRef":"1",
    "InternRef":"int1",
    "Reference":"Reference Device",
    "Description":"MyDescription "
    },{
    "idRef":"2",
    "InternRef":"int2",
    "Reference":"Reference Device",
    "Description":"MyDescription "
    }
    
    
    ]
 }
 
 
 
 
 */
$i=0;
if($Access->getLevel()>0 && isset($json["action"])){
    $sql="SELECT idRef,internRef, Reference,description,Status,SMT,id_type FROM reference where  1  order by internRef ASC ;";
    $res= query($sql);
    $list=array();
    $list+=["action" => "ref_list" ];
    $list+=["devices" => [] ];
    while($req=mysqli_fetch_array($res)){
        $l=array();
        $l+=["idRef" => $req["idRef"] ];
        $l+=["Reference" => $req["Reference"] ];
        $l+=["internRef" => $req["internRef"] ];
        $l+=["description" => $req["description"] ];
        
        if( $req["SMT"]!= null){
            $l+=["SMT" => $req["SMT"] ];
        }
        
        $l+=["id_type" => $req["id_type"] ];
        
        switch($req["Status"]){
            default:
                $l+=["Status" => "unknown" ];
                break;
            case 0:
                $l+=["Status" => "In production" ];
                break;
            case 1:
                $l+=["Status" => "Not Recommended for new designs" ];
                break;
            case 2:
                $l+=["Status" => "End of life" ];
                break;
            
        }
        //echo json_encode($l) ."\n\n";
        
        $list["devices"][$i]=$l;
        $i++;
        
    }
    
    echo  json_encode($list) ;
    
}


?>