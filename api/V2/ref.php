<?php
include_once '../../conex.ini.php';
include_once "../../include.php";
include_once "../../classAccess.php";
include_once "../../classPart.php";

/*
 Example :
 
 
 //send
{"action":"ref","devices":[{"idRef":"200"}]}
{"action":"ref","devices":[{"InternRef":"RES0805_1K"}]}
{"action":"ref","devices":[{"Reference":"NE555"}]}


//answers 
 *  {
  "action": "ref",
  "devices": [
    {
      "idRef": "105",
      "Reference": "CB30SI/15-CI",
      "internRef": "NEW_200",
      "description": "CONVERTISSEUR DC/DC 28V/15V - 30W",
      "prices": [{
          "id_price": "55",
          "supplierName": "RS",
          "supplierReference": "357-1319",
          "quantity": null,
          "UnitPrice": null,
          "Link": "https://fr.rs-online.com/web/p/link/357-1319/"
        },{
          "id_price": "150",
          "supplierName": "RS",
          "supplierReference": "357-1319",
          "quantity": null,
          "UnitPrice": null,
          "Link": "https://fr.rs-online.com/web/p/link/357-1319/"
        }]
    }
  ]
}
  
  
//2 request in the same time 
 * 
{"action":"ref","devices":[{"idRef":"200"},{"idRef":"201"}]}

{
  "action": "ref",
  "devices": [
    {
      "idRef": "200",
      "Reference": "CB30SI/15-CI",
      "internRef": "NEW_200",
      "description": "CONVERTISSEUR DC/DC 28V/15V - 30W",
      "prices": []
    },
    {
      "idRef": "201",
      "Reference": "CB30SM/12-CI",
      "internRef": "NEW_201",
      "description": "CONVERTISSEUR DC/DC 28V/12V - 30W",
      "prices": []
    }
  ]
}

 
 
 
 
 */

$list=array();
if($Access->getLevel()>0 && isset($json["action"]) && isset($json["devices"]) ){
    
    $list+=["action" => "ref" ];
    $list+=["devices" => [] ];
    $i=0;
    
    foreach ($json["devices"] as $val){

        $part=new classPart();
        
        if(isset($val["idRef"] )){
            $part=new classPart($val["idRef"]);
        }
        if(isset($val["InternRef"] )){
            $part->findInternRef($val["InternRef"]);
        }
        if(isset($val["Reference"] )){
            $part->findReference($val["Reference"]);
        }
        $part->readOctopart();
        $part->update_price();
        $l=array();
        
        if($part->reference!=NULL && $part->internRef !=NULL && $part->Description !=NULL){
            $l+=["idRef" => $part->getidRef() ];
            $l+=["Reference" => $part->reference ];
            $l+=["internRef" => $part->internRef ];
            $l+=["description" => $part->Description ];
            $l+=["prices" => $part->readPrice()] ;
            
            $list["devices"][$i]=$l;
            $i++;
        }
        
    }
    
    
}

echo  json_encode($list) ;

?>