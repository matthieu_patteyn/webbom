<?php

include_once "conex.ini.php";
include_once "include.php";
include_once "classDeviceJson.php";


function PageHeader($type=0,$name=""){
    global $CDevice;
    global $CONFIG;
    
    
    $linkLeft="";
    $linkRight="";
    $title="WebBom";
    
    if($name!=""){
        $name=": $name";
    }
    
    switch($type){
        
        case "project.php":
            $title="Project $name-WebBom";
            break;
        case "bom.php":
            $title="Bom $name-WebBom";
            break;
        case "ListPartNumber.php":
            $title="ListPartNumber -WebBom";
            break;
        case "ref.php":
            $title="Ref $name-WebBom";
            break;
            
    }
    
    echo"<head>\n";
    echo"<title>$title</title>\n";
    echo"<script src=\"script.js\"></script>\n" ;
    echo"<link rel=\"stylesheet\" href=\"site.css\">\n";
    echo"<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>\n";
    echo"<link href=\"bootstrap/css/bootstrap.css\" rel=\"stylesheet\">\n";
    echo"<script src=\"bootstrap/js/bootstrap.js\"></script>\n";
    echo"</head>\n";
    
    switch($type){
        
        default:
            
            
            
            if(isset($_SESSION["level"])){
                $linkLeft="<a href=\"newReference.php\">New Reference</a>-<a href=\"ListPartNumber.php\">List Part Number</a>";
                $linkRight="<a href=\"admin.php\">Admin</a>-<a href=\"user.php\">Config</a>-<a href=\"index.php?logout=1\">Logout</a>\n";
            }
            else{
                $linkLeft="\n";
                $linkRight="\n";
            }
            
            
    }
    if(isset($_SESSION["level"])){
        echo"  	<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
        	<a class=\"navbar-brand\" href=\"index.php\">WebBom</a>
        	<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        	   <span class=\"navbar-toggler-icon\"></span>
        	</button>
            
        	<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
            	<ul class=\"navbar-nav mr-auto\">";
        if($CONFIG["CAOModule"]!=0){
       echo"         	<li class=\"nav-item\">
                	   <a class=\"nav-link\" href=\"newReference.php\">CAO newRef</a>
                	</li>";
        }
                	
       echo"             <li class=\"nav-item dropdown\">
                    	<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    	   Part Number
                    	</a>
                    	<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                    	   <a class=\"dropdown-item\" href=\"ListPartNumber.php\">All</a>
                    	<div class=\"dropdown-divider\"></div>\n";
        
                            foreach ($CDevice->list_device as $i => $name){
                                echo" <a class=\"dropdown-item\" href=\"ListPartNumber.php?id_type=$i\">$name</a>\n";
                            }
        
        
        
        
        
        echo"
                    	</div>
                	</li>";
        
       echo" <li class=\"nav-item dropdown\">
                    	<a class=\"nav-link dropdown-toggle\" href=\"#\" id=\"navbarDropdown\" role=\"button\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    	   Library
                    	</a>
                    	<div class=\"dropdown-menu\" aria-labelledby=\"navbarDropdown\">
                    	   <a class=\"dropdown-item\" href=\"ListPartNumber.php?lib=\">Unknown</a>
                    	<div class=\"dropdown-divider\"></div>\n";
        
       
       $sql="SELECT DISTINCT(`lib`) FROM `reference` WHERE lib!=\"\" and lib is not null order by lib asc";
       
       $res= query($sql);
       while($req=mysqli_fetch_array($res)){
               echo" <a class=\"dropdown-item\" href=\"ListPartNumber.php?lib=".$req[0]."\">".$req[0]."</a>\n";
           
        }
        
        
        
        
        
        echo"
                    	</div>
                	</li>";


        echo"             	</ul>\n\n";
        $value_submit="";
		if(isset($_GET["submit"]) and isset($_GET["where"])){
			$value_submit=protect($_GET["where"]);
		}
		
    	echo"      <form class=\"form-inline my-2 my-lg-0\" action=\"ListPartNumber.php\" method=\"get\">
                	<input class=\"form-control mr-sm-2\" type=\"search\" placeholder=\"Search\" aria-label=\"Search\" name=\"where\" value=\"$value_submit\">
                	<input type=\"hidden\" name=\"submit\" value=\"1\">
					<button class=\"btn btn-outline-success my-2 my-sm-0\" type=\"submit\">Search</button>
                	</form>
            
                	<ul class=\"navbar-nav mx-lg-0\">
                	<li class=\"nav-item\">
                	   <a class=\"nav-link\" href=\"admin.php\">Admin</a>
                	</li>
            
                	<li class=\"nav-item\">
                	   <a class=\"nav-link\" href=\"user.php\">User</a>
                	</li>
                	<li class=\"nav-item\">
                	   <a class=\"nav-link\" href=\"index.php?logout=1\">Logout</a>
                	</li>
            	</ul>
        	</div>
        	</nav>\n";
    }
    else{
        {
            echo"  	<nav class=\"navbar navbar-expand-lg navbar-dark bg-dark\">
        	<a class=\"navbar-brand\" href=\"index.php\">WebBom</a>
        	<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">
        	   <span class=\"navbar-toggler-icon\"></span>
        	</button>
                
        	<div class=\"collapse navbar-collapse\" id=\"navbarSupportedContent\">
                
        	</div>
        	</nav>\n";
        }
    }
    
}


?>