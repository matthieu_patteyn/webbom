<?php
// Page created by Matthieu Patteyn mpatteyn@ambisense.net
include_once "conex.ini.php";
include_once "include.php";



class classAccess{
	
	private $level=0;
	private $id_user=0;
	private $API_user="";
	public 	$login ="";
	public $id_compagny=0;
	

	public function __construct() {
	    $this->init_var();
	}
	private function init_var(){
		$this->level=0;
		$this->id_user=0;
		$this->id_compagny=0;
		
		if(isset($_SESSION["level"])){
			$this->level=$_SESSION["level"];
		}
		if(isset($_SESSION["id_user"])){
			$this->id_user=$_SESSION["id_user"];
		}	
		if(isset($_SESSION["id_compagny"])){
			$this->id_compagny=$_SESSION["id_compagny"];
		}
		
		if($this->id_user==0){
		    if(isset($_COOKIE["WEBBOM_KEY"]) && $_COOKIE["WEBBOM_KEY"]!=""){
		        $this->connectAPI($_COOKIE["WEBBOM_KEY"]);
		        $this->update_SESSION();
		    }
		}
	}
	
	public function readAll(){
		$sql="SELECT API_password, login FROM user where id_user=".$this->id_user.";";
		$res= query($sql);
		if($req=mysqli_fetch_array($res)){
			$this->API_user= $req[0];
			$this->login=$req[1];
		}
	}
	
	public function read_id_user(){	
		return $this->id_user;
	}
	
	public function readAPI(){	
		if($this->API_user==""){
			$this->readAll();
		}
		return $this->API_user;
	}
	public function readLogin(){	
		if($this->login==""){
			$this->readAll();
		}
		return $this->login;
	}
	
	public function connectAPI($PassAPI){
		$PassAPI=protect($PassAPI);
		$sql="SELECT id_user,level,pass,login, id_compagny FROM user where API_password=\"$PassAPI\" ;";
		
		$res= query($sql);
		if($req=mysqli_fetch_array($res)){
		    $this->id_user=$req["id_user"];
			$this->level=$req["level"];
			$this->id_compagny=1;
		}
	}
	
	public function update_SESSION(){
	    $_SESSION["id_user"]=$this->id_user;
	    $_SESSION["level"]=$this->level;
	    $_SESSION["id_compagny"]=$this->id_compagny;
	}
	
	public function connect($login,$pass){
		$login=protect($login);
		$pass=protect($pass);
		$password_hash=password_hash($pass,PASSWORD_DEFAULT);
		
		echo"<!--Pass= $password_hash \n-->";
		
		$sql="SELECT id_user,level,pass ,API_password  FROM user where login=\"$login\" ;";
		$res= query($sql);
		if($req=mysqli_fetch_array($res)){
			if(password_verify ( $pass, $req["pass"])){
			    $this->id_user=$req["id_user"];
			    $this->level=$req["level"];
			    $this->id_compagny=1;
				$this->update_SESSION();
				
				setcookie("WEBBOM_KEY",$req["API_password"],time()+3600*24*30);
				return 1;
			}
			return 0;//Bad password
			
			
		}
		return 0;
		
	}
	
	public function getLevel(){
		return ($this->level);
	}
	
	
	public function index_php(){
	    if($this->level>10 ){
			return 1;
		}
		else{
			return 0;
		}
		
	}
	
	public function logout(){
		// Finally, destroy the session.
		session_destroy();
		setcookie("WEBBOM_KEY","",time()-1);
		session_start();
		$this->init_var();
	}
	
	public function NewPassword($pass){
	    $pass=protect($pass);
	    if(strlen($pass)>4){
	        $pass=password_hash($pass,PASSWORD_DEFAULT);
	        $sql="UPDATE user SET pass ='$pass'where id_user=$this->id_user;";
	        query($sql);
	        return 1;
	        
	    }
	    return 0;
	    
	}
	
	public function newUser($Login,$Password,$Level){
	    $Login=protect($Login);
	    $Password=protect($Password);
	    $Level=protect($Level);
	    if(strlen($Password)>4 && strlen($Login)>3){
	        $apiVAL=uniqid();
	        $Password=password_hash($Password,PASSWORD_DEFAULT);
	        $sql="INSERT INTO user (login,name,pass,level,id_compagny,API_password)
			VALUES (\"".$Login."\" ,\"".$Login."\",\"$Password\",\"$Level\",\"1\",\"$apiVAL\");";
	        query($sql);
	        return 1;
	    }
	    return 0;
	}
	
	
}

$Access=new classAccess();

if(isset($_GET["logout"])){
	$Access->logout();
}


?>