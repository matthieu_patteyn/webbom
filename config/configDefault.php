<?php
$CONFIG=array();
///////////////////////////////////////
//Default config Do not modify , if you want to modify create config.php



///////////////////////////////
//WebBom configuration 

$octopart_api_key="6faa0cf9";
$folder="WebBom";



//UPLOAD FILE
$CONFIG["uploadFile"]=1;
$CONFIG["uploadFileTypes"]=["jpg","png","jpeg","gif","pdf"];
$CONFIG["uploadFileSize"]=800000;
$CONFIG["uploadFileLevelMin"]=10;




//Project Module [BETA]
$CONFIG["ProjectModule"]=0;


//CAO [BETA]
$CONFIG["CAOModule"]=0;



//SMT (ClassPart)

$CONFIG["list_NO_SMT"]=["TO-92","TO-220","DIP","TO-3","ITO-220","DIP","PDIP"];
$CONFIG["list_SMT"]=["TO-263-3","SOT-23","TO-263","TO-263-3", "DFN","SOIC", "0402", "0603" ,"0805","1206", "TO-252"];



if(file_exists("config.php")){
    include_once("config.php");
}
?>