<?php
include_once ('include.php');
//Todo Create class 
class class_config_device{
    public $list_device=array();
    
    public function __construct(){
        $str = file_get_contents('config/device.json',FILE_USE_INCLUDE_PATH);
        $json = json_decode($str, true);
        foreach ($json['list_device'] as $value) {
            if(isset($value["id"]) && isset($value["name"]) ){
                $this->list_device+=[protect($value["id"]) => protect($value["name"]) ];
            }
        }
    }
}






$CDevice=new class_config_device();


//Need to do : Create buffer
function JSON_read_name($id_type){
    $str = file_get_contents('config/device.json',FILE_USE_INCLUDE_PATH);
    $json = json_decode($str, true);
    foreach ($json['list_device'] as $value) {
        if($value["id"]==$id_type){
            if(isset($value["name"])){
                return protect($value["name"]);
            }
        }
    }
    return "";
}

function JSON_read_octopart_name($id_type){
    $str = file_get_contents('config/device.json',FILE_USE_INCLUDE_PATH);
    $json = json_decode($str, true);
    
    $res=array();
    
    foreach ($json['list_device'] as $value) {
        if($value["id"]==$id_type){
            foreach ($value["specs"] as $data) {
                if($data["octopart"]!=""){
                    $res+= [protect($data["octopart"]) => protect($data["name"]) ]; 
                }
                
            }
        }
    }
    return $res;
}


function JSON_read_spec_name($id_type){
    $str = file_get_contents('config/device.json',FILE_USE_INCLUDE_PATH);
    $json = json_decode($str, true);
    
    $res=array();
    
    foreach ($json['list_device'] as $value) {
        if($value["id"]==$id_type){
            foreach ($value["specs"] as $data) {
                $res+= [protect($data["name"]) => "1" ];
            }
        }
    }
    return $res;
}

function JSON_read_unit($id_type){
    $str = file_get_contents('config/device.json',FILE_USE_INCLUDE_PATH);
    $json = json_decode($str, true);
    
    $res=array();
    
    foreach ($json['list_device'] as $value) {
        if($value["id"]==$id_type){
            foreach ($value["specs"] as $data) {
                if(isset($data["unit"])){
                    $res+= [protect($data["name"]) => protect($data["unit"]) ];
                }
                else{
                    $res+= [protect($data["name"]) => "" ];
                }
            }
        }
    }
    return $res;
}





?>