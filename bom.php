<?php
include_once 'conex.ini.php';
include_once "include.php";
include_once "classAccess.php";
include_once "classBOM.php";
include_once "header.php";
include_once "classFileUpload.php";

PageHeader("bom.php");
$script="";

$ref=0;
if(isset($_GET["ref"])){
	$ref=protect($_GET["ref"]);
}



$bom=new classBOM($ref);
$ref=$bom->getid_bom();

if(isset($_GET["modif_bom_information"]) and isset($_GET["information"])){
	
	$bom->updateInformation($_GET["information"]);
	
}

if(isset($_GET["read_only"])){
	$bom->setReadOnly();
}

echo"<table width=\"100%\"><tr><td width=\"50%\" style=\"vertical-align:top;\">";

echo"<form action=\"bom.php\" method=\"get\">
<input type=\"hidden\" name=\"ref\" Value=\"$ref\">
<input type=\"hidden\" name=\"modif_bom_information\" Value=\"1\">
<table border=1>
<tr><td>Project name:</td><td><a href=\"project.php?ref=".$bom->id_project."\">".$bom->getProjectName()."</a></td></tr>
<tr><td>Project description:</td><td>".$bom->getProjectDescription()."</td></tr>
<tr><td>Revision:</td><td>".$bom->id_revision."</td></tr>
<tr><td>Information:</td><td><input type=\"text\" name=\"information\" Value=\"".$bom->information."\"></td></tr>
<tr><td>Date Last revision:</td><td>".$bom->date_revision."</td></tr>\n";

if($bom->getReadOnly()==0){
	
	echo"<tr><td></td><td><a href=\"bom.php?ref=$ref&read_only=1\">Read/Write</a></td></tr>\n";
}

echo"<tr><td></td><td><input type=\"submit\" Value=\"Update\"></td></tr>\n";
echo "</table></form>\n\n";

echo"</td><td style=\"vertical-align:top;\">";

echo"File:<br>Check List ( Verif prod / First prod / TEST EMC ...)";


//File
$cUpload= new classFileUpload();
if($CONFIG["uploadFile"]!=0){
    if(isset($_POST["submit_file"])) {
        $ans=$cUpload->new_file_bom($_FILES["fileToUpload"],$bom->getid_bom(),protect($_POST["device_type"]));
        echo $ans["text"];
    }
}

$listFileDevice=$cUpload->list_file_bom($bom->getid_bom());
echo"<table border=1 style=\"width:100%\">\n";
foreach ($listFileDevice as $val){
    echo"<tr><td>".$val["login"]."</td><td>".date("j/m/Y")."</td><td><a href=\"".$val["link"]."\" target=\"_blank\">".$val["fileName"]."</a></td><td><a href=\"".$val["link"]."\" target=\"_blank\">".$val["category"]."</a></td></tr>";
}
echo"</table>";

if($CONFIG["uploadFile"]!=0){
    echo"<form action=\"bom.php?ref=".$bom->getid_bom()."\" method=\"post\" enctype=\"multipart/form-data\">
	<table style=\"width:100%\" border =1>
	<tr><td>File:</td><td><input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\"></td></tr>
	<tr><td>Category</td><td><select name=\"device_type\"   style=\"width:100%\">\n";
    $str = file_get_contents('./config/CategoryBom.json',FILE_USE_INCLUDE_PATH);
    $json = json_decode($str, true);
    foreach ($json['Category'] as $value) {
        echo"<option value=\"".$value."\"";
        echo">". $value ."</option>\n";
    }
    echo"</select></td></tr>
	<tr><td></td><td><input type=\"submit\" value=\"Upload\" name=\"submit_file\"></td></tr>
	</table>
        
</form>";
}

echo"</div>";


echo"</td></table>";


//BOM 


?>
<script>

var PriceSum=0;
var quantitySum=0;
var quantitySMTSum=0;

function UpdateWarning(bomRef,ligne,value) {
	if(value==true){
		value=1;
	}
	else{
		value=0;
	}
	var link= 'api/V1/api.php?type=bom&action=update_warning&bomRef='+bomRef +'&ligne='+ ligne +'&value='+ value;
	sendAPI(link);
}

function Updateinfo(bomRef,ligne,value){
	var link= 'api/V1/api.php?type=bom&action=update_info&bomRef='+bomRef +'&ligne='+ ligne +'&value='+ value;
	sendAPI(link);
}

function update_price(col,ref,quantity,SMT){
	var jsonFile=sendAPI('api/V1/api.php?type=ref&ref='+ref+'&action=info');
	var mydata = JSON.parse(jsonFile);
	if (typeof mydata.price[0] !== 'undefined'){
		document.getElementById('supplierName_'+col).innerHTML=mydata.price[0].supplierName;
		document.getElementById('supplierReference_'+col).innerHTML=mydata.price[0].supplierReference;
		document.getElementById('supplierquantity_'+col).innerHTML=mydata.price[0].supplierquantity;
		document.getElementById('UnitPrice_'+col).innerHTML=mydata.price[0].UnitPrice;

		if(mydata.price[0].Link!=""){
			document.getElementById('Link_'+col).innerHTML='<a href=\''+mydata.price[0].Link+'\'>'+mydata.price[0].Link+'</a>';
		}
		else{
			document.getElementById('Link_'+col).innerHTML=mydata.price[0].Link;
		}
		
		document.getElementById('TotalPrice_'+col).innerHTML=mydata.price[0].UnitPrice*quantity;

		PriceSum=PriceSum+mydata.price[0].UnitPrice*quantity;

	}
	quantitySMTSum=quantitySMTSum+SMT*quantity;
	quantitySum=quantitySum+quantity;
	document.getElementById('sum_quantity').innerHTML=quantitySum;
	document.getElementById('sum_Price').innerHTML=PriceSum;
	document.getElementById('sum_SMT').innerHTML=quantitySMTSum;
	
}
	
	



		

</script>


<?php 

if(isset($_GET["Add_reference"])and isset($_GET["internRef"]) and isset($_GET["references"]) and isset($_GET["quantity"])){
	$part=$bom->findPart($_GET["internRef"]);
	$bom->listAddReference($_GET["references"],$_GET["quantity"],$part);
}


echo"<table border=1 width=\"100%\" id=\"headerTable\">";
echo"<tr><td>References</td><td>quantity</td><td>internRef</td><td>description</td><td>SMT</td><td>Status</td><td>info</td><td>warning</td>  ";
echo" <td>supplierName</td> <td>supplierReference</td> <td>supplierquantity</td><td>supplierquantity</td><td>UnitPrice</td><td>Link</td><td>TotalPrice</td>   </tr>\n";
$sql="SELECT bom_list.id ,bom_list.ref,bom_list.quantity,bom_list.info, bom_list.warning , reference.idRef, reference.internRef,reference.description,reference.SMT, reference.Status FROM reference, bom_list where reference.idRef=bom_list.idRef and bom_list.idBom=$ref;";
$i=0;
$res= query($sql);
while($req=mysqli_fetch_array($res)){
	$id=$req[0];
	$References=$req[1];
	$quantity=$req[2];
	$info=$req[3];
	$warning =$req[4];
	
	
	$idRef=$req[5];
	$internRef=$req[6];
	$description=$req[7];
	$SMT =$req[8];
	$Status=$req[9];
	
	$checked_warning="";
	$warning_info="";
	if($warning){
		$checked_warning="checked";
		$warning_info="1";
	}
	$Valobsolete="";

	if($Status==2 || $Status==1 ){
		echo"<tr style=\"background-color:red;\">";
		if($Status==2){
		    $Valobsolete="Obsolete";
		}
	}
	elseif ($warning==1 || $Status==-1){
		echo"<tr style=\"background-color:orange;\">";
	}
	else{
		echo"<tr>";
	}
	
	echo "<td>$References</td>";
	echo"<td>$quantity</td>";
	echo"<td><a href=\"ref.php?ref=$idRef\">$internRef</a></td><td>$description</td><td>$SMT</td>";
	echo"<td>$Valobsolete</td>";
	
	echo"<td><font class=\"hide\">$info</font><input type=\"text\" onchange=\"Updateinfo('$ref','$id',this.value);\" value=\"$info\" ></td>";	
	echo "<td><font class=\"hide\">$warning_info</font><input type=\"checkbox\" $checked_warning onchange=\"UpdateWarning('$ref','$id',this.checked);\"></td>";
	
	echo"<td id=\"supplierName_$i\"></td>";
	echo"<td id=\"supplierReference_$i\"></td>";
	echo"<td id=\"supplierquantity_$i\"></td>";
	echo"<td id=\"supplierquantity_$i\"></td>";
	echo"<td id=\"UnitPrice_$i\"></td>";
	echo"<td id=\"Link_$i\"></td>";
	echo"<td id=\"TotalPrice_$i\"></td>";

	echo "</tr>\n";
	
	
	$script.="update_price($i,$idRef,$quantity,$SMT);\n";
	$i++;
	
}

echo"<tr><td></td><td id=\"sum_quantity\">0</td><td></td><td></td><td id=\"sum_SMT\"></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td id=\"sum_Price\"></td></tr>\n";


if($bom->getReadOnly()==0){
	echo"<tr><form action=\"bom.php\" method=\"GET\">
	<input type=\"hidden\" name=\"ref\" value=\"$ref\" >
	<input type=\"hidden\" name=\"Add_reference\" value=\"1\" >
	<td><input type=\"text\" name=\"references\" value=\"\" ></td>
	<td><input type=\"text\" name=\"quantity\" value=\"\" ></td>
	<td><input type=\"text\" name=\"internRef\" value=\"\" ></td>
	<td><input type=\"submit\"  value=\"Insert\" ></td>
	<td></td>
	<td></td>
	<td></td>
	<td></td></form></tr>\n";
}


echo"</table>\n\n";


echo"<script>$script</script>\n";
echo"<button  onclick=\"ExcelReport(document.getElementById('headerTable'));\"> EXPORT </button>";

echo"<br><br>";

?>