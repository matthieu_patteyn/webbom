CREATE TABLE `bom` (
  `idBom` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_project` int(10) unsigned NOT NULL,
  `id_revision` int(11) NOT NULL,
  `information` text NOT NULL,
  `date_rev` date NOT NULL,
  `readOnly` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idBom`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
CREATE TABLE `bom_list` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idBom` int(10) unsigned NOT NULL,
  `idRef` int(10) unsigned NOT NULL,
  `ref` text NOT NULL,
  `quantity` float NOT NULL,
  `info` text,
  `warning` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
CREATE TABLE `compagny` (
  `id_compagny` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_compagny`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `price` (
  `id_price` int(11) NOT NULL AUTO_INCREMENT,
  `idRef` int(11) NOT NULL,
  `supplierName` text NOT NULL,
  `supplierReference` text NOT NULL,
  `quantity` float DEFAULT NULL,
  `UnitPrice` float DEFAULT NULL,
  `Link` text NOT NULL,
  PRIMARY KEY (`id_price`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `project` (
  `id_project` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `id_compagny` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id_project`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
CREATE TABLE `reference` (
  `idRef` int(11) NOT NULL AUTO_INCREMENT,
  `internRef` text NOT NULL,
  `Reference` text,
  `Description` text,
  `SMT` int(11) DEFAULT NULL,
  `lib` text,
  `id_type` int(11) NOT NULL DEFAULT '0',
  `Status` int(11) NOT NULL DEFAULT '-1',
  `unitPrice` decimal(10,3) DEFAULT NULL,
  `observation` text,
  `Manufacturer` text,
  `ManufacturerPartNo` text,
  `id_compagny` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idRef`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
CREATE TABLE `reference_cao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idRef` int(11) NOT NULL,
  `cao` int(11) NOT NULL DEFAULT '0',
  `Part` text NOT NULL,
  `Value` text NOT NULL,
  `Footprint` text NOT NULL,
  `id_compagny` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
CREATE TABLE `reference_spec` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idRef` int(11) NOT NULL,
  `name_spec` text NOT NULL,
  `value_spec` text NOT NULL,
  `unit_spec` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
CREATE TABLE `upload` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_type` int(11) NOT NULL,
  `type_ID` int(11) NOT NULL COMMENT 'id_project or id_ref',
  `category` text NOT NULL,
  `time_upload` int(11) NOT NULL,
  `link` text NOT NULL,
  `id_user` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `mail` varchar(45) DEFAULT NULL,
  `API_password` text,
  `id_compagny` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;