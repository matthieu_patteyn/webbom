<?php
include_once "classAccess.php";
include_once 'include.php';
include_once 'classDeviceJson.php';
include_once "classPrice.php";

class classPart{
	private $idRef=0;
	public $internRef="";
	public $Description="";
	public $SMT="";
	public $unitPrice="";
	public $Manufacturer="";
	public $ManufacturerPartNo="";
	public $id_type="0";
	public $status="-1";//0= Ready , 1=Not Recommended for New Designs , 2 = obsolete, -1 = need to update
	public $reference="";
	public $observation="";
	public $lib="";
	public $Cprice=array();

	
	
	
	public function __construct($ref=0) {
		global $Access;
		if($Access->getLevel()>1){
			$ref=protect($ref);
			$this->idRef=$ref;
			$this->updatePart();
		}
	}
	public function getidRef(){
		return $this->idRef;
	}
	
	private function updatePart(){
		$sql="select internRef,Reference, Description, SMT,  Manufacturer , ManufacturerPartNo,id_type, Status , lib , unitPrice, observation from reference where idRef=\"".$this->idRef."\";";
		$res= query($sql);
		$req=mysqli_fetch_array($res);
		$this->internRef=$req["internRef"];
		$this->reference=$req["Reference"];
		$this->Description=$req["Description"];
		$this->SMT=$req["SMT"];
		$this->Manufacturer=$req["Manufacturer"];
		$this->ManufacturerPartNo=$req["ManufacturerPartNo"];
		$this->id_type=$req["id_type"];
		$this->status=$req["Status"];
		$this->lib=$req["lib"];
		if($this->SMT==NULL){
		    $this->SMT="";
		}
		$this->unitPrice=$req["unitPrice"];
		$this->observation=$req["observation"];
		if($this->unitPrice==NULL){
		    $this->unitPrice="";
		}
	}
	
	public function NewPartNumber(){
		global $Access;
		if($Access->getLevel()>1){
			$sql="SELECT max(idRef) FROM reference where 1;";
			$res= query($sql);
			$req=mysqli_fetch_array($res);
			$i=$req[0]+1;
			
	       
			
			$sql="INSERT INTO reference (internRef,observation)
				VALUES ( \"NEW_$i\",\"\");";
			query($sql);
			$sql="SELECT max(idRef) FROM reference where 1;";
			$res= query($sql);
			$req=mysqli_fetch_array($res);
			$this->idRef=$req[0];
			$this->updatePart();
			
			return $this->idRef;
			
		}
	}
	
	public function savePart(){
		$internRef=protect($this->internRef);
		$reference=protect($this->reference);
		$Description=protect($this->Description);
		$SMT=protect($this->SMT);
		$Status=protect($this->status);
		$lib=protect($this->lib);
		$unitPrice=protect($this->unitPrice);
		$observation=protect($this->observation);
		$UPDATE="";
		if($SMT!=""){
		    $UPDATE.=" , SMT=\"$SMT\"";
		}
		else{
		    $UPDATE.=" , SMT=null ";
		}
		
		if($unitPrice!=""){
		    $UPDATE.=" , unitPrice=\"$unitPrice\"";
		}
		else{
		    $UPDATE.=" , unitPrice=null ";
		}
		
		if(isset($_SESSION["id_compagny"])){
		    
		    $UPDATE.= " ,id_compagny=\"" . $_SESSION["id_compagny"] . "\" " ;
		    
		}
		
		$Manufacturer=protect($this->Manufacturer);
		$ManufacturerPartNo=trim(protect($this->ManufacturerPartNo));
		$id_type=protect($this->id_type);
		
		$sql="UPDATE Reference SET internRef = \"$internRef\" , Reference=\"$reference\", Description = \"$Description\",  Manufacturer=\"$Manufacturer\", 
                ManufacturerPartNo=\"$ManufacturerPartNo\",lib=\"$lib\" , id_type=\"$id_type\" , Status=\"$Status\", observation=\"$observation\"  $UPDATE  
                WHERE idRef=\"".$this->idRef."\"; ";
		query($sql);
		
		$this->updatePart();
	}
	
	
	
	public function CAODeleteRef($id){
		$id=protect($id);
		$sql="DELETE from reference_cao where idRef=\"".$this->idRef."\" and id=\"$id\"";
		query($sql);
	}
	
	public function CAOMoveRef($id,$newRef){
		$id=protect($id);
		$newRef=protect($newRef);
		
		$sql="UPDATE reference_cao SET idRef=\"$newRef\"
			where idRef=\"".$this->idRef."\" and id=\"$id\"";
		query($sql);
		
	}
	
	public function priceAdd($supplierName,$supplierReference,$quantity,$UnitPrice,$Link){
	    global $db_link;
		if($this->idRef!=0){
			$supplierName=protect($supplierName);
			$supplierReference=protect($supplierReference);
			$quantity=protect($quantity);
			$UnitPrice=protect($UnitPrice);
			$Link=protect($Link);
			
			
			if($quantity!=0){
			    $sql="INSERT INTO price (idRef, supplierName, supplierReference,quantity,UnitPrice,Link)
				VALUES (\"".$this->idRef."\", \"$supplierName\", \"$supplierReference\",\"$quantity\",\"$UnitPrice\",\"$Link\");";
			    query($sql);
			}
			else{
			    $sql="INSERT INTO price (idRef, supplierName, supplierReference,Link)
				VALUES (\"".$this->idRef."\", \"$supplierName\", \"$supplierReference\",\"$Link\");";
			    query($sql);
			}
				
			
			
			

		}
		
		
	}
	
	public function priceDelete($id_price){
		$id_price=protect($id_price);
		$sql="DELETE from price where idRef=\"".$this->idRef."\" and id_price=\"$id_price\"";
		query($sql);
	}
	
	public function readPrice(){
	    $sql="SELECT * FROM price where idRef=" . $this->idRef . ";";
	    $res= query($sql);
	    
	    $price = [];
	    $i=0;
	    
	    while($req=mysqli_fetch_array($res)){
	        $l=array();
	        $l+=["id_price" => $req["id_price"] ];
	        $l+=["idRef" => $req["idRef"] ];
	        $l+=["supplierName" => $req["supplierName"] ];
	        $l+=["supplierReference" => $req["supplierReference"] ];
	        $l+=["quantity" => $req["quantity"] ];
	        $l+=["UnitPrice" => $req["UnitPrice"] ];
	        $l+=["Link" => $req["Link"] ];
	        $price[$i] =$l;
	        $i=$i+1;
	    }
	    
	    return $price;
	}
	
	
	public function update_price(){
	    $sql="SELECT id_price FROM price where idRef=" . $this->idRef . ";";
	    $res= query($sql);
	    
	    while($req=mysqli_fetch_array($res)){
	        $this->Cprice = new classPrice($req["id_price"]);
	        
	    }
	        
	}
	

	public function readOctopart($update=1){
	    GLOBAL $CONFIG;
	    $update=protect($update);
	    global $octopart_api_key;
	    try {
    	    $list_important_spec=JSON_read_spec_name($this->id_type);
    	    $list_important_spec_unit=JSON_read_unit($this->id_type);
    	    
    	    $ret=array();
    	    $listVariable=JSON_read_octopart_name($this->id_type);
    	    $link="https://octopart.com/api/v3/parts/match?queries=[{\"sku\":\"".$this->ManufacturerPartNo."\"}]&apikey=".$octopart_api_key."&pretty_print=true&include[]=specs";
    	    if(isset($_SESSION["DebugSQL"]) && $_SESSION["DebugSQL"]==1){
    	        echo "\n\n<!-- Debug : " . $link . " -->\n";
    	    }
    	    $str = @file_get_contents($link);
    	    $json = json_decode($str, true);
    	    if(isset($json['results'])){
    	        foreach ($json['results'] as $result){
    	            if(isset($result['items']['0']['specs'])){
    	                foreach ($result['items']['0']['specs'] as $name_octopart =>$value) {
    	                    $list=array();
    	                    $name="";
    	                    $val="";
    	                    if(isset($value["value"][0])){
    	                        $val=$value["value"][0];
    	                    }
    	                    else if(isset($value["max_value"])){
    	                        $val=$value["max_value"];
    	                    }
    	                    else if(isset($value["min_value"])){
    	                        $val=$value["min_value"];
    	                    }
    	                    $unit="";
    	                    if(isset($value["metadata"]["unit"]["symbol"])){
    	                        $unit=$value["metadata"]["unit"]["symbol"];
    	                    }
    	                    if(isset($listVariable[$name_octopart])){
    	                        $name=$listVariable[$name_octopart];
    	                        $list+= ["update" => "1" ];
    	                    }
    	                    else{
    	                        $list+= ["update" => "0" ];
    	                    }
    	                    if(isset($list_important_spec[$name])){
    	                        $list_important_spec[$name]="0";
    	                    }
    	                    $list+= ["name" => $name ];
    	                    $list+= ["name_octopart" => $name_octopart ];
    	                    $list+= ["value" => $val ];
    	                    $list+= ["unit" => $unit ];
    	                    ///////////////////////////////////////////
    	                    //Auto Update
    	                    if($name_octopart=="lifecycle_status"){
    	                        if($val=="Not Recommended for New Designs" || $val=="Pending Obsolescence" ){
    	                            if($this->status==0|| $this->status==-1){
    	                                $this->status=1;
    	                                $this->savePart();
    	                            }
    	                        }
    	                        if($val=="Active"){
    	                            if($this->status==-1){
    	                                $this->status=0;
    	                                $this->savePart();
    	                            }
    	                        }
    	                        $list+= ["important" => "1" ];
    	                    }
    	                    else{
    	                        $list+= ["important" => "0" ];
    	                    }
    	                    
    	                    if($this->SMT=="" ){
    	                        
    	                        if($name_octopart=="mounting_style"){
    	                            if($val=="Surface Mount"){
    	                                $this->SMT="1";
    	                                $this->savePart();
    	                                
    	                            }
    	                            if($val=="Through Hole" || $val=="PCB" ){
    	                                $this->SMT="0";
    	                                $this->savePart();
    	                            }
    	                        }
    	                        
    	                        
    	                        if( $name_octopart=="case_package"){
    	                            
    	                            foreach ($CONFIG["list_NO_SMT"]as $l){
    	                                if($l==$val){
    	                                    $this->SMT="0";
    	                                    $this->savePart();
    	                                }
    	                            }
    	                            foreach ($CONFIG["lis_SMT"] as $l){
    	                                if($l==$val){
    	                                    $this->SMT="1";
    	                                    $this->savePart();
    	                                }
    	                            }
    	                        }
    	                    }
    	                    
    	                    
    	                    
    	                    //////////////////////////////////////////
    	                    array_push($ret, $list);
    	                    if($update!=0 && $name!=""){
    	                        $sql="DELETE from reference_spec where idRef=\"".$this->idRef."\" and name_spec=\"$name\" ";
    	                        query($sql);
    	                        $sql="INSERT INTO reference_spec (idRef, name_spec, value_spec ,unit_spec)
    	                      VALUES (\"".$this->idRef."\", \"$name\", \"$val\",\"$unit\");";
    	                        query($sql);
    	                    }
    	                    
    	                    
    	                    
    	                    
    	                }
    	            }
    	        }
    	    }
    	    
    	    
    	    
    	    
    	    $sql="SELECT `name_spec`,value_spec, unit_spec FROM `reference_spec` WHERE idRef=".$this->idRef.";";
    	    $res= query($sql);
    	    
    	    while($req=mysqli_fetch_array($res)){
    	        if(isset($list_important_spec[$req[0]])){
    	            if($list_important_spec[$req[0]]==1){
    	                $list=array();
    	                $list+= ["update" => "1" ];
    	                $list+= ["name" =>     $req[0] ];
    	                $list+= ["name_octopart" => "" ];
    	                $list+= ["value" =>    $req[1] ];
    	                $list+= ["unit" =>     $req[2] ];
    	                $list+= ["important" => "0" ];
    	                array_push($ret, $list);
    	                $list_important_spec[$req[0]]=0;
    	            }
    	        }
    	    }
    	    foreach ($list_important_spec as $name => $l){
    	        if($l==1){
    	            $list=array();
    	            $list+= ["update" => "1" ];
    	            $list+= ["name" =>     $name ];
    	            $list+= ["name_octopart" => "" ];
    	            $list+= ["value" =>    "" ];
    	            $list+= ["important" => "0" ];
    	            if(isset($list_important_spec_unit[$name])){
    	                $list+= ["unit" =>  $list_important_spec_unit[$name] ];
    	                $unit=$list_important_spec_unit[$name];
    	            }
    	            else{
    	                $list+= ["unit" =>     "" ];
    	                $unit="";
    	            }
    	            $sql="INSERT INTO reference_spec (idRef, name_spec, value_spec ,unit_spec)
    	                      VALUES (\"".$this->idRef."\", \"$name\", \"\",\"$unit\");";
    	            query($sql);
    	            array_push($ret, $list);
    	        }
    	    }
    	    
    	    
    	    
    	    return $ret;
	    }
	    catch (Exception $e){
	        if(isset($_SESSION["debug"])){
	            
	            echo"bug readOctopart";
	        }
	        
	        
	    }
	}
	
	
	public function updateSpec($name,$val,$unit){
	    $name=protect($name);
	    $val=trim(protect($val));
	    $unit=trim(protect($unit));
	    
	    $sql="UPDATE reference_spec
			SET value_spec = \"$val\" , unit_spec = \"$unit\" 
			WHERE idRef=\"".$this->idRef."\" and name_spec = \"$name\""; 
		query($sql);
	    
	    
	}
	
	
	public function findInternRef($internRef){
	    $internRef=protect($internRef);
	    $sql="SELECT idRef FROM reference where internRef=\"$internRef\";";
	    $res= query($sql);
	    if($req=mysqli_fetch_array($res)){
	        $this->idRef=$req[0];
	        $this->updatePart();
	        return $this->idRef;
	    }
	    return 0;
	}
	
	public function findReference($Reference){
	    $Reference=protect($Reference);
	    $sql="SELECT idRef FROM reference where Reference=\"$Reference\";";
	    
	    $res= query($sql);
	    if($req=mysqli_fetch_array($res)){
	        $this->idRef=$req[0];
	        $this->updatePart();
	        return $this->idRef;
	    }
	    return 0;
	}
	
	
}



?>