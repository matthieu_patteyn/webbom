<?php
include_once "classAccess.php";
include_once 'include.php';


class classPrice{
    private $idPrice=0;
    public $supplierName="";
    public $supplierReference="";
    public $quantity="";
    public $UnitPrice="";
    public $Link="";

    
    
    
    public function __construct($ref=0) {
        global $Access;
        if($Access->getLevel()>1){
            $ref=protect($ref);
            $this->idPrice=$ref;
            $this->readDB();
        }
    }
    
    public function readDB(){
        $sql="SELECT * FROM price where id_price=".$this->idPrice.";";
        
        $res= query($sql);
        
        if($req=mysqli_fetch_array($res)){
            $this->supplierName=$req["supplierName"];
            $this->supplierReference=$req["supplierReference"];
            $this->quantity=$req["quantity"];
            $this->UnitPrice=$req["UnitPrice"];
            $this->Link=$req["Link"];
        }
        
        
        if($this->Link==""){
            if($this->supplierName=="RS" or $this->supplierName=="RADIOSPARES"){
                //https://fr.rs-online.com/web/p/link/2152591/
                $this->supplierName="RS";
                $this->Link="https://fr.rs-online.com/web/p/link/".$this->supplierReference."/";
                $this->updateDB();
            }
            
            if($this->supplierName=="farnell" or $this->supplierName=="FARNELL" or $this->supplierName=="Farnell" or $this->supplierName=="FARNEL"  ){
                //http://fr.farnell.com/search?st=412-983
                $this->supplierName="farnell";
                $this->Link="http://fr.farnell.com/search?st=".$this->supplierReference."";
                $this->updateDB();
            }
            
            
            if($this->supplierName=="mouser" or $this->supplierName=="MOUSER" or $this->supplierName=="Mouser"){
                //https://www.mouser.fr/Search/Refine.aspx?Keyword=584-LT8609EMSE_PBF
                $this->supplierName="mouser";
                $this->Link="https://www.mouser.fr/Search/Refine.aspx?Keyword=".$this->supplierReference."";
                $this->updateDB();
            }
            
            
            
            
            
            
            
        }
    }
    public function updateDB(){
        $update_sql="";
        $this->supplierName=protect($this->supplierName);
        $this->supplierReference=protect($this->supplierReference);
        
        $this->quantity=protect($this->quantity);
        
        if($this->quantity!=""){
            $update_sql=", quantity=\"".$this->quantity."\",";
        }
        
        $this->supplierReference=protect($this->supplierReference);
        $this->UnitPrice=protect($this->UnitPrice);
        if($this->UnitPrice!=""){
            $update_sql="UnitPrice = \"".$this->UnitPrice."\",";
        }
        
        
        $this->Link=protect($this->Link);
        
        
        $sql="UPDATE price SET supplierName = \"".$this->supplierName."\" , supplierReference=\"".$this->supplierReference."\", 
             Link = \"".$this->Link."\"    $update_sql   
                WHERE id_price=\"". $this->idPrice."\"; ";
        query($sql);
        
    }
    
    
    
    
    
}

?>