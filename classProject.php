<?php

include_once "classAccess.php";
include_once 'include.php';

class classProject{
	private $id_project=0;
	public $name="";
	public $description="";

	
	public function __construct($id_project=0) {
		global $Access;
		if($Access->getLevel()>1){
			$id_project=protect($id_project);
			$this->id_project=$id_project;
			$this->update();
		}
	}
	
	private function update(){
		
		$sql="SELECT name,description FROM project where id_project=\"".$this->id_project."\";";
		$res= query($sql);
		if($req=mysqli_fetch_array($res)){
			$this->name=$req[0];
			$this->description=$req[1];
		}
	}
	
	public function getRef(){
		return $this->id_project;
		
	}
	
	public function updateProject($name,$description){
		
		if($this->id_project!=0){
			$name=protect($name);
			$description=protect($description);
			
			$sql="UPDATE project SET name = \"$name\", description = \"$description\"
			WHERE id_project=\"".$this->id_project."\"; ";
			query($sql);
			
			$this->update();
		}
		
	}
	
	public function find_project($name){
		global $Access;
		if($Access->getLevel()>1){
			$name=protect($name);
			$sql="SELECT id_project FROM project where  name =\"".$name."\";";
			$res= query($sql);
			if($req=mysqli_fetch_array($res)){
				$this->id_project=$req[0];
			}
			$this->update();
			return $this->id_project;
		}
		return 0;
	}
	//Create one project if you don't have one with the same name 
	public function new_project($ProjectName="NewProject",$ProjectDescription=""){
		global $Access;
		if($Access->getLevel()>1){
			if($this->find_project($ProjectName)==0){
				$ProjectName=protect($ProjectName);
				$ProjectDescription=protect($ProjectDescription);
				
				$sql="INSERT INTO project (name,description,id_compagny)
				VALUES (\"$ProjectName\",\"$ProjectDescription\",".$Access->id_compagny.");";
				query($sql);
				$this->find_project($ProjectName); 
			}
		}
		return $this->id_project;
		
		
		
	}
	
	
}

?>