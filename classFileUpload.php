<?php
include_once "classAccess.php";
include_once 'include.php';
include_once 'classDeviceJson.php';
include_once "classPrice.php";

class classFileUpload{
	public function __construct($ref=0) {
		global $Access;
		if($Access->getLevel()>1){

		}
	}
	
	public function new_file_device($FILE,$id_ref,$category){
		return $this->new_file($FILE, 1 ,$id_ref,$category);
	}
	public function new_file_bom($FILE,$id_ref,$category){
		return $this->new_file($FILE, 2 ,$id_ref,$category);
	}
	
	
	public function new_file($FILE,$type,$type_id,$category) {
		global $Access,$CONFIG;
		$ans="";
		if($Access->getLevel()>=$CONFIG["uploadFileLevelMin"] and $CONFIG["uploadFile"]==1){
			$time=time();
			$target_dir = "upload/".$time."/";
			createPath($target_dir);
			$target_file = $target_dir . basename($FILE["name"]);
			
			
			$uploadOk = 1;
			$FileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			if (file_exists($target_file)) {
				$ans.= "Sorry, file already exists.<br>";
				$uploadOk = 0;
			} 
			if ($FILE["size"] > $CONFIG["uploadFileSize"]) {
				$ans.= "Sorry, your file is too large.<br>";
				$uploadOk = 0;
			}

			if($uploadOk==1){
				$uploadOk=0;
				$types= "";
				foreach ($CONFIG["uploadFileTypes"] as $value){
					if($FileType==$value){
						$uploadOk=1;
					}
					$types.=" , " .$FileType;
				}
				if($uploadOk==0){
					$ans.= "Sorry, only $types files are allowed.<br>";
				}
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				$ans.= "Sorry, your file was not uploaded.<br>";
			// if everything is ok, try to upload file
			} else {
				if (move_uploaded_file($FILE["tmp_name"], $target_file)) {
					$ans.= "The file ". basename( $FILE["name"]). " has been uploaded.<br>";
					$sql="INSERT INTO `upload` ( `id_type`,  `type_ID`, `category`, `time_upload`, `link`, `id_user` , `fileName`) 
						VALUES ('$type', '$type_id', '$category', '$time', '$target_file', '".$Access->read_id_user()."' , '".$FILE["name"] ."'); ";
					query($sql);
					
				} else {
					$ans.= "Sorry, there was an error uploading your file.<br>";
				}
			}
		}
		$answers=array();
		$answers["text"]=$ans;
		$answers["ok"]=$uploadOk;
		
		return $answers;
	}
	
	public function list_file_device($id_ref){
		return $this->list_file(1,$id_ref);
	}
	public function list_file_bom($id_ref){
		return $this->list_file(2,$id_ref);
	}
	public function list_file($type,$type_id){
		$answers=array();
		$sql="SELECT `upload`.`id`,`upload`.`category`,`upload`.`time_upload`,`upload`.`link`,`user`.`login` , upload.fileName
		FROM `upload`, `user` 
		WHERE `id_type`=$type and `type_ID`=$type_id and `user`.`id_user`=`upload`.`id_user` 
		ORDER BY `category` ASC, `time_upload` DESC;";
		
		$answers=array();
		$i=0;
		$res= query($sql);
	    while($req=mysqli_fetch_array($res)){
	        $l=array();
			$l+=["category" => $req[1] ];
			$l+=["time" => $req[2] ];
			$l+=["link" => $req[3] ];
			$l+=["login" => $req[4] ];
			$l+=["fileName" => $req[5] ];
			$answers[$i]= $l;
			$i++;
	    }
		
		
		return $answers;
	}
	
	
}

?>