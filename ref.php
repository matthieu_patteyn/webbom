<?php
include_once 'conex.ini.php';
include_once "include.php";
include_once "classAccess.php";
include_once "classPart.php";
include_once "classPrice.php";
include_once "header.php";
include_once "classFileUpload.php";



$ref=0;
if(isset($_GET["ref"])){
	$ref=protect($_GET["ref"]);
}



$part=new classPart($ref);

PageHeader("ref.php",$part->internRef);

//////////////////////////////////////////////////////////////////////////////////
//CSS
?>
<style type="text/css">

#Ref_general_information{
float:left;
width:50%;

}


#ref_sub_information{
float:Right;
width:50%;

}

#Ref_information{
width:100%;
}


#Ref_CAO{
float:left;
width:100%;
}
#Ref_PRICE{
float:left;
width:100%;

}
#Ref_SPEC{
float:left;
width:100%;

}

textarea
{
  border:1px solid #999999;
  width:100%;
  margin:5px 0;
  padding:3px;
}


</style>





<?php 



if(isset($_GET["NewPartNumber"])){
	$part->NewPartNumber();
	if(isset($_GET["device_type"])){
	    $part->id_type=protect($_GET["device_type"]);
	}
	
	
	
	echo"New Part Number!<br>\n";
	
}
$ref=$part->getidRef();

//////////////////////////////////////////////////////////////////////////////////////
//General information
echo"<div id=\"Ref_information\">\n";
echo"<div id=\"Ref_general_information\">\n";



if(isset($_GET["modif"]) && $_GET["modif"]=="Reference"){
	if(isset($_GET["internRef"])){
	    $part->internRef=$_GET["internRef"];  
	}
	if(isset($_GET["Description"])){
	    $part->Description=$_GET["Description"];
	}
	if(isset($_GET["SMT"])){
	    $part->SMT=$_GET["SMT"];
	}
	if(isset($_GET["Manufacturer"])){
	    $part->Manufacturer=$_GET["Manufacturer"];
	}
	if(isset($_GET["ManufacturerPartNo"])){
	    $part->ManufacturerPartNo=$_GET["ManufacturerPartNo"];
	}

	if(isset($_GET["device_type"])){
	    $part->id_type=$_GET["device_type"];
	}
	if(isset($_GET["status"])){
	    $part->status=$_GET["status"];
	}
	if(isset($_GET["Reference"])){
	    $part->reference=$_GET["Reference"];
	}
	if(isset($_GET["lib"])){
	    $part->lib=$_GET["lib"];
	}
	if(isset($_GET["observation"])){
	    $part->observation=$_GET["observation"];
	}
	if(isset($_GET["unitPrice"])){
	    $part->unitPrice=$_GET["unitPrice"];
	}
	$part->savePart();
	echo"Updated value!<br>\n";
}



echo"<table style=\"width:100%\">
<form action=\"ref.php\" method=\"get\">
<input type=\"hidden\" name=\"ref\" Value=\"$ref\">
<input type=\"hidden\" name=\"modif\" Value=\"Reference\">
<tr><td>Intern Ref:</td><td><input type=\"text\" style=\"width:100%\" name=\"internRef\" Value=\"".$part->internRef."\"></td></tr>
<tr><td>Reference:</td><td><input type=\"text\" style=\"width:100%\" name=\"Reference\" Value=\"".$part->reference."\"></td></tr>
<tr><td>Description:</td><td><input type=\"text\" style=\"width:100%\" name=\"Description\" Value=\"".$part->Description."\"></td></tr>
<tr><td>Device Type:</td><td><select name=\"device_type\"   style=\"width:100%\">\n";


$str = file_get_contents('./config/device.json',FILE_USE_INCLUDE_PATH);
$json = json_decode($str, true);
foreach ($json['list_device'] as $value) {
    
    echo"<option value=\"".$value["id"]."\"";
    
    if($value["id"]==$part->id_type ){
        
        echo"selected";
    }
    
    echo">". $value["name"] ."</option>\n";
}

echo"

</select>
</td></tr>
<tr><td>SMT:</td><td><input type=\"text\" style=\"width:100%\" name=\"SMT\" Value=\"".$part->SMT."\"></td></tr>";

//Status 
if($part->status==0 ){
    echo"<tr>";
}
else{
    echo"<tr bgcolor=\"#E60000\">";
}
echo"<td>Status:</td><td><select name=\"status\"   style=\"width:100%\">";

echo"<option value=\"-1\"";

if($part->status==-1 ){
    echo"Need to update";
}
echo">Need to update</option>\n";

echo"<option value=\"0\"";

if($part->status==0 ){ 
    echo"selected";
}
echo">In Production</option>\n";

echo"<option value=\"1\"";

if($part->status==1 ){
    echo"selected";
}
echo">Not Recommended for new designs</option>\n";

echo"<option value=\"2\"";

if($part->status==2 ){
    echo"selected";
}
echo">End of life</option>\n";
echo"</select></td></tr>";

//END status

echo"<tr><td>Library:</td><td><input type=\"text\" style=\"width:100%\" name=\"lib\" Value=\"".$part->lib."\"></td></tr>
<tr><td>UnitPrice:</td><td><input type=\"text\" style=\"width:100%\" name=\"unitPrice\" Value=\"".$part->unitPrice."\"></td></tr>
<tr><td>Manufacturer:</td><td><input type=\"text\" style=\"width:100%\" name=\"Manufacturer\" Value=\"".$part->Manufacturer."\"></td></tr>
<tr><td>ManufacturerPartNo:</td><td><input type=\"text\" style=\"width:100%\" name=\"ManufacturerPartNo\" Value=\"".$part->ManufacturerPartNo."\"></td></tr>
<tr><td></td><td><input type=\"submit\" Value=\"Update\"></td></tr>
</table>\n</div>\n";




////////////////////////////////////////////

echo"<div id=\"ref_sub_information\">";

echo"<table style=\"width:100%\">
        <tr><td style=\"width:38px\">Observation:</td><td>​<textarea name=\"observation\" id=\"txtArea\" rows=\"5\" >".$part->observation."</textarea></td></tr>
        <tr><td></td><td><input type=\"submit\" Value=\"Update\"></td></tr>";


echo"</form>\n";
echo"</table>";

//File
$cUpload= new classFileUpload();
if($CONFIG["uploadFile"]!=0){
	if(isset($_POST["submit_file"])) {
		$ans=$cUpload->new_file_device($_FILES["fileToUpload"],$part->getidRef(),protect($_POST["device_type"]));
		echo $ans["text"];
	}
}

$listFileDevice=$cUpload->list_file_device($part->getidRef());
echo"<table border=1 style=\"width:100%\">\n";
foreach ($listFileDevice as $val){
	echo"<tr><td>".$val["login"]."</td><td>".date("j/m/Y")."</td><td><a href=\"".$val["link"]."\" target=\"_blank\">".$val["fileName"]."</a></td><td><a href=\"".$val["link"]."\" target=\"_blank\">".$val["category"]."</a></td></tr>";
}
echo"</table>";
	
if($CONFIG["uploadFile"]!=0){
	echo"<form action=\"ref.php?ref=".$part->getidRef()."\" method=\"post\" enctype=\"multipart/form-data\">
	<table style=\"width:100%\" border =1>
	<tr><td>File:</td><td><input type=\"file\" name=\"fileToUpload\" id=\"fileToUpload\"></td></tr>
	<tr><td>Category</td><td><select name=\"device_type\"   style=\"width:100%\">\n";
	$str = file_get_contents('./config/CategoryFile.json',FILE_USE_INCLUDE_PATH);
	$json = json_decode($str, true);
	foreach ($json['Category'] as $value) {
		echo"<option value=\"".$value."\"";
		echo">". $value ."</option>\n";
	}
	echo"</select></td></tr>
	<tr><td></td><td><input type=\"submit\" value=\"Upload\" name=\"submit_file\"></td></tr>
	</table>

</form>";
}

echo"</div>";
echo"</div>";//end general information

//////////////////////////////////////////////////////////////////////////////////////////

if(isset($_GET["DeleteRefCAO"])){
	$part->CAODeleteRef($_GET["DeleteRefCAO"]);
	
}

if(isset($_GET["Modif_refCAO"])   and isset($_GET["id_refCAO"]) and  isset($_GET["New_id_ref"]) ){
	
	$part->CAOMoveRef($_GET["id_refCAO"], $_GET["New_id_ref"]);
}

//////////////////////////////////////////////////////////////////////////////////////
echo"<div id=\"Ref_CAO\">";
echo"CAO Reference<br>\n\n";


echo"<table border=1>
<tr><td>ID</td><td>Part</td><td>Value</td><td>Footprint</td><td>Id_ref<br>(Move)</td><td>Delete</td></tr>\n";

$sql="SELECT * FROM reference_cao where idRef=\"$ref\" ";
$res= query($sql);
while($req=mysqli_fetch_array($res)){
	
	
	$id_refCAO=$req["id"];
	
	
	echo"<form action=\"ref.php\" method=\"get\">
	<input type=\"hidden\" name=\"ref\" Value=\"$ref\">
	<input type=\"hidden\" name=\"Modif_refCAO\" Value=\"1\">
	<input type=\"hidden\" name=\"id_refCAO\" Value=\"$id_refCAO\">
	<tr><td>$id_refCAO</td><td>".$req["Part"]."</td><td>".$req["Value"]."</td><td>".$req["Footprint"]."</td><td><input type=\"text\" name=\"New_id_ref\" Value=\"$ref\"></td><td><a href=\"ref.php?ref=$ref&DeleteRefCAO=$id_refCAO\">Delete</a></td></tr>
	</form>\n";
}
echo"</table>\n</div>\n";





//////////////////////////////////////////////////////////////////////////////////////

echo"<div id=\"Ref_PRICE\">";
echo"Price<br>\n";


if(isset($_GET["newPrice"])  and isset($_GET["supplierName"])  and isset($_GET["supplierReference"])  and isset($_GET["quantity"])  and isset($_GET["UnitPrice"])  and isset($_GET["Link"])){
	$part->priceAdd($_GET["supplierName"],$_GET["supplierReference"],$_GET["quantity"],$_GET["UnitPrice"],$_GET["Link"]);
	echo"ADD!<br>\n";
}

if(isset($_GET["price_del"])){
	
	$price_del=$_GET["price_del"];
	$part->priceDelete($price_del);
	
	echo"Delete!<br>\n";
}


echo"<table border=1 style=\"width:100%\">
<tr><td>id</td><td>supplierName</td><td>supplierReference</td><td>quantity</td><td>UnitPrice</td><td>Link</td><td></td></tr>\n";



$sql="SELECT * FROM price where idRef=$ref;";

$res= query($sql);
while($req=mysqli_fetch_array($res)){
	echo"<tr><td>".$req["id_price"]."</td><td>".$req["supplierName"]."</td><td>".$req["supplierReference"]."</td><td>".$req["quantity"]."</td><td>".$req["UnitPrice"]."</td>";
	echo"<td><a href=\"".$req["Link"]."\">".$req["Link"]."</a></td>";
	echo"<td><a href=\"ref.php?ref=$ref&price_del=".$req["id_price"]."\">Remove</a></td></tr>";
	
	if($req["Link"]=="" or $req["Link"]==null){
	    $cPrice=new classPrice($req["id_price"]);
	}
	
	
}






echo"<form action=\"ref.php\" method=\"get\">
<input type=\"hidden\" name=\"ref\"  Value=\"$ref\">
<input type=\"hidden\" name=\"newPrice\" Value=\"1\">
<tr><td></td><td><input type=\"text\" style=\"width:100%\" name=\"supplierName\"></td>
<td><input type=\"text\" style=\"width:100%\" name=\"supplierReference\"></td>
<td><input type=\"text\" style=\"width:100%\" name=\"quantity\"></td>
<td><input type=\"text\" style=\"width:100%\" name=\"UnitPrice\"></td>
<td><input type=\"text\" style=\"width:100%\" name=\"Link\"></td>
<td><input type=\"submit\" style=\"width:100%\" value=\"New\"></td></tr>
</form>\n";

echo"</table>\n</div>\n";

/////////////////////////////////////////////////////
//Octopart 

echo"<div id=\"Ref_SPEC\">";
echo"<a href=\"https://octopart.com/search?q=" .$part->Manufacturer. " "  .$part->ManufacturerPartNo."\">Device information (Octopart)</a><a href=\"https://octopart.com/api/v3/parts/match?queries=[{&quot;sku&quot;:&quot;".$part->ManufacturerPartNo."&quot;}]&apikey=".$octopart_api_key."&pretty_print=true&include[]=specs\">[API]</a>";

//others price 

if(isset($_GET["update_spec"])){
    for($i=0;$i<$_POST["nb_data"];$i++){
        $part->updateSpec($_POST["name_$i"],$_POST["val_$i"],$_POST["unit_$i"]);
    }
	
	
}
$i=0;
echo"<form action=\"ref.php?ref=$ref&update_spec=1\" method=\"post\">\n";
echo"<table border=1>";
$table=$part->readOctopart();
foreach ($table as $value){
    if($value["update"]==1){
        if($value["important"]=="1"){
            echo"<tr bgcolor=\"LightGray\">";
        }
        else{
            echo"<tr>";
        }
        echo"<td><input type=\"hidden\" name=\"name_$i\" value=\"".$value["name"]."\">".$value["name"]."</td>
            <td>".$value["name_octopart"]."</td>
            <td><input type=\"text\" name=\"val_$i\" value=\"".$value["value"]."\"></td>
            <td><input type=\"text\" name=\"unit_$i\" value=\"".$value["unit"]."\"></td></tr>\n\n";
        $i++;
    }
    else{
        if($value["important"]=="1"){
            echo"<tr bgcolor=\"LightGray\">";
        }
        else{
            echo"<tr>";
        }
        echo"<td>".$value["name"]."</td><td>".$value["name_octopart"]."</td><td>".$value["value"]."</td><td>".$value["unit"]."</td></tr>\n";
    }
    
}
echo"</table><input type=\"hidden\" name=\"nb_data\" value=\"".$i."\"><input type=\"submit\"  value=\"Update\"></form></div>";

//specs (usefull)


?>