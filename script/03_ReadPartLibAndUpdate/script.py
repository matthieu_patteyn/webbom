from csvReader import readCSV
from api import api_add_reference, api_add_price, api
import json


class import_data:
    def __init__(self):
        #configuration 
        
        self.link="ParLib.csv"
        self.libDefaultValue=""
        self.IDDeviceType="0"
        
        self.header={}
        self.header["InternRef"]="Code ECA"
        self.header["Reference"]="Nom"
        self.header["Description"]="Designation"
        self.header["SMT"]=0
        self.header["Manufacturer"]="Fabricant"
        self.header["ManufacturerPartNo"]="Ref. Fab."
        self.header["lib"]=0
        #price
        
        self.header["supplierName1"]="Fournisseur"
        self.header["supplierReference1"]="Ref. Four."
        self.header["quantity1"]=0
        self.header["UnitPrice1"]=0
        self.header["Link1"]=0
        
        self.header["supplierName2"]="Fournisseur"
        self.header["supplierReference2"]="Ref. Four."
        self.header["quantity2"]=0
        self.header["UnitPrice2"]=0
        self.header["Link2"]=0
        
        self.header["supplierName3"]="Fournisseur"
        self.header["supplierReference3"]="Ref. Four."
        self.header["quantite3"]=0
        self.header["UnitPrice3"]=0
        self.header["Link3"]=0
        
        
        ###########################################################
        
        self.header_pos={}
        for (i, item) in enumerate(self.header):
            self.header_pos[item]=0
            
        self.initValue()
            
    def initValue(self):
        self.value={}
        
        for (i, item) in enumerate(self.header):
            self.value[item]=""
        
        
        if(self.value["lib"]==""):    
            self.value["lib"]=self.libDefaultValue

        
    
    def read_header(self):
        i=1
        while(i<50):
            title=readCSV(self.link,1,i)
            for (j, item) in enumerate(self.header):
                
                if(self.header[item]==title and self.header_pos[item]==0):
                    double=0
                    for (k, item2) in enumerate(self.header_pos):
                        if(i==self.header_pos[item2]):
                            double=1
                    if(double==0):
                        self.header_pos[item]=i
                    
            i=i+1
    def updatePart(self,idRef=-10):
        res={"action":"ref"}
        res["devices"]=[]
        devices={"idRef":str(idRef)  }
        res["devices"].append(devices)
        api_=api()
        jsonData=json.dumps(res)
        res=api_.send(jsonData)
        
    def import_all(self):
        i=2
        while(i<500000):
            self.initValue()
            for (j, item) in enumerate(self.header_pos):
                if(self.header_pos[item]!=0):
                    self.value[item]=readCSV(self.link,i,self.header_pos[item])
            
            
            if(self.value["Reference"]!=""  and self.value["Description"]=="" and self.value["InternRef"]=="" and self.value["SMT"]=="" and self.value["Manufacturer"]=="" and self.value["ManufacturerPartNo"]==""):
                self.value["lib"]=self.value["Reference"]
                self.libDefaultValue=self.value["Reference"]
            
            
            

            
            
            if(self.value["Reference"]!="" and self.value["Description"]!="" ):
                api=api_add_reference()
                if(self.value["InternRef"]!=""):
                    api.InternRef=self.value["InternRef"]
                if(self.value["Reference"]!=""):
                    api.Reference=self.value["Reference"]
                if(self.value["Description"]!=""):
                    api.Description=self.value["Description"]
                if(self.value["SMT"]!=""):
                    api.SMT=self.value["SMT"]
                if(self.value["Manufacturer"]!=""):
                    api.Manufacturer=self.value["Manufacturer"]
                if(self.value["ManufacturerPartNo"]!=""):
                    api.ManufacturerPartNo=self.value["ManufacturerPartNo"]
                if(self.value["lib"]!=""):
                    api.lib=self.value["lib"]
                    
                    
                if(self.IDDeviceType!=""):
                    api.IDDeviceType=self.IDDeviceType
                
                
                
                id=api.insert_ref()
            
                print("Add: "+ str( id ) )
                
                
                id=int(id)
                
                if(id!=0):
                    k=1
                    while(k<=3):
                        if(self.value["supplierName"+str(k)]):
                            api= api_add_price(id)
                            api.supplierName=self.value["supplierName"+str(k)]
                            api.supplierReference=self.value["supplierReference"+str(k)]
                            api.quantity=self.value["quantity"+str(k)]
                            api.UnitPrice=self.value["UnitPrice"+str(k)]
                            
                            api.Link=self.value["Link"+str(k)]
                            api.insert_price()
                        k=k+1
                        
                    
                    self.updatePart(id) # Octopart update 
            
            i=i+1
        


importData=import_data()
importData.read_header()
importData.import_all()
