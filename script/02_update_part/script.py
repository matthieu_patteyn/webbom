from api import api
import json


class update_part:
    def __init__(self):
        #configuration 
        self.api=api()
        
    def updatePart(self,idRef=-10):
        res={"action":"ref"}
        res["devices"]=[]
        devices={"idRef":str(idRef)  }
        res["devices"].append(devices)
        api_=api()
        jsonData=json.dumps(res)
        res=api_.send(jsonData)
        
    
    def updateAll(self):
        res={"action":"ref_list"}
        i=0

        
        res=self.api.send(json.dumps(res))
        res=json.loads(res)
        
        for val in res["devices"]:
            print( " ID " + str(i) +" - IdRef" +  str(val["idRef"])  )
            self.updatePart(val["idRef"])
            i=i+1





update_part=update_part()
update_part.updateAll()
