import requests
import json
#API webBom 


def removeSpecialChar(data):
    data=data.replace("&", "_")
    data=data.replace("#", "_")
    if(len(data)>0):
        if(data[0:1]=="." or data[0:1]=="_"):
            return data[1:]
    return data

class api:
    def __init__(self):
        self.url='http://localhost/webbom/'
        self.key="fzfjezohfzeiufhixahdyogacyohuinyaozbxtaxoixbdgjkeg45646324874"
        self.debug=1
    
    def send(self,jsonData=""):
        
        send=self.url+ "api/V2/?API_login="+self.key+"&queries="+jsonData
        r=requests.get(send)
        if(self.debug):
            print("API SEND url: " + send + "\n")
            print("Answers:"+ r.text)
        return  r.text

    def test(self):
        res=json.dumps({"action":"test"})
        print(self.send(res))
        



class api_add_reference():
    def __init__(self):
        self.api=api()
        self.init()
    def init(self):
        self.InternRef=""
        self.Reference=""
        self.Description=""
        self.SMT=""
        self.Manufacturer=""
        self.ManufacturerPartNo=""
        self.lib=""
        self.IDDeviceType=""
    def insert_ref(self):
        res={"action":"insert_ref"}
        if(self.InternRef!=""):
            res["InternRef"]=removeSpecialChar(self.InternRef)
        res["Reference"]=removeSpecialChar(self.Reference)
        res["Description"]=removeSpecialChar(self.Description)
        if(self.SMT!=""):
            res["SMT"]=self.SMT
        res["Manufacturer"]=removeSpecialChar(self.Manufacturer)
        res["ManufacturerPartNo"]=removeSpecialChar(self.ManufacturerPartNo)
        res["lib"]=removeSpecialChar(self.lib)
        if(self.IDDeviceType!=""):
            res["IDDeviceType"]=removeSpecialChar(self.IDDeviceType)
        
        res=self.api.send(json.dumps(res))
        res=json.loads(res)
        if(res["action"]=="insert_ref"):
            return res["idRef"]
        return 0
        
class api_add_price():
    def __init__(self,id_device):
        self.api=api()
        self.id_device=id_device
        self.init()
    def init(self):
        self.supplierName=""
        self.supplierReference=""
        self.quantity=""
        self.UnitPrice=""
        self.Link=""
    

    
    def insert_price(self):
        res={"action":"insert_price"}
        res["id_device"]=self.id_device
        res["price"]={}
        
        if(self.supplierName!=""):
            res["price"]["supplierName"]=removeSpecialChar(self.supplierName)
        if(self.supplierReference!=""):
            res["price"]["supplierReference"]=removeSpecialChar(self.supplierReference)
        if(self.quantity!=""):
            res["price"]["quantity"]=removeSpecialChar(self.quantity)
        if(self.UnitPrice!=""):
            res["price"]["UnitPrice"]=removeSpecialChar(self.UnitPrice)
        if(self.Link!=""):
            res["price"]["Link"]=removeSpecialChar(self.Link)
            
        
        res=self.api.send(json.dumps(res))
        res=json.loads(res)

    



#a=api()
#a.debug=1
#a.test()

#a=api_reference()
#a.InternRef="TEST"
#a.Reference="TEST"
#a.Description="testDescription"
#a.SMT="0"
#a.Manufacturer=""
#a.ManufacturerPartNo=""
#a.lib=""

#a.insert_ref()


#a=api_price(91)
#a.supplierName="RS"
#a.supplierReference="DDD"

#a.insert_price()

