<?php
// Page created by Matthieu Patteyn mpatteyn@ambisense.net
session_start();
include_once "conex.ini.php";
include_once("config/configDefault.php");

$WebBomVersion="0.0.2";


if(isset($_GET["debug"])){
	if($_GET["debug"]==1)
		$_SESSION["DebugSQL"]=1;
	else
		$_SESSION["DebugSQL"]=0;
		
	
}

function protect($data){
	global $magic_quote;
	global $db_link;
	if($magic_quote==1){
		//protected message  
		return $data;
	}
	else{
		
		return htmlspecialchars($data);
	}
} 



function query($sql){
	global $db_link;
	if(isset($_SESSION["DebugSQL"]) && $_SESSION["DebugSQL"]==1){
		echo "\n\n<!-- Debug SQL: " . $sql . " -->\n";
	}
	$data=mysqli_query($db_link,$sql);
	return $data;
	
}



function cao_name($id){
	switch ($id){
		case 1:
			return "KiCad";
			break;
		default:
			return "Unknow-".$id;
			break;
		
	}
	
}

/** 
 * recursively create a long directory path
 */
function createPath($path) {
    if (is_dir($path)) return true;
    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
    $return = createPath($prev_path);
    return ($return && is_writable($prev_path)) ? mkdir($path) : false;
}

?>